/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.changer;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.time.StopWatch;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.CreateTableResult;
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement;
import com.amazonaws.services.dynamodbv2.model.KeyType;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException;
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType;

import br.com.vr.schemachanger.data.domain.NovaPessoa;
import br.com.vr.schemachanger.data.domain.Pessoa;
import br.com.vr.schemachanger.data.repository.NovaPessoaRepository;
import br.com.vr.schemachanger.data.repository.PessoaRepository;
import lombok.extern.slf4j.Slf4j;

/**
 * Classe responsável pela imlpementacao das mudanças na tabela Pessoa.
 * 
 * @author Verity
 *
 */
@Service
@Slf4j
public class PessoaChangerImpl implements PessoaChanger {

    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private NovaPessoaRepository novaPessoaRepository;

    @Autowired
    private AmazonDynamoDB amazonDynamoDB;

    @Autowired
    private ModelMapper modelMapper;

    @Value("${amazon.dynamodb.pessoa.destination-table}")
    private String destination;

    private static final String SIGLA_EMISSOR = "SiglaEmissor";
    private static final String CNPJ_CPF = "CnpjCpf";
    private static final String CANNOT_CREATE_PREEXISTING_TABLE = "Cannot create preexisting table";
    private static final String TABLE_ALREADY_EXISTS = "Table already exists:";

    @Override
    public boolean copiaDadosTabelaPessoa() {

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        log.info("Inicia copia dos registros da tabela pessoa para a tabela destino: {}", destination);
        MutableBoolean flagCriacao = new MutableBoolean();

        if (this.criacaoTabelaDestino(flagCriacao)) {

            if (flagCriacao.isTrue()) {
                log.info("Reexecute o programa.");
                return false;
            }

            log.info("Carregando registros da tabela origem.");
            modelMapper.getConfiguration().setSkipNullEnabled(true);

            List<Pessoa> registrosTabela = pessoaRepository.findAll();
            log.info("Tempo gasto fazendo scan na tabela origem: {}", stopWatch.getTime());
            List<NovaPessoa> registrosNovaTabela = new ArrayList<>();
            registrosTabela.stream().forEach(pessoa -> {
                NovaPessoa novaPessoa = new NovaPessoa();
                modelMapper.map(pessoa, novaPessoa);
                registrosNovaTabela.add(novaPessoa);
            });
            log.info("Tempo gasto copiando as propriedades: {}", stopWatch.getTime());

            log.info("Salvando registros na tabela destino.");
            
            try {
                novaPessoaRepository.saveAll(registrosNovaTabela);
            } catch (ResourceNotFoundException e) {
                log.info("Tabela criada, reexecute o programa.");
                return false;
            }

            log.info("Tempo gasto inserindo registros na tabela destino: {}", stopWatch.getTime());

            log.info("Fim do processo");
            log.info("Quantidade de registros lidos: {}", registrosTabela.size());
            log.info("Quantidade de registros processados: {}", registrosNovaTabela.size());
        }

        log.info("Tempo gasto em milis: {}", stopWatch.getTime());

        return false;
    }

    /**
     * Método responsável por criar uma tabela de destino.
     * 
     * @param destination
     */
    private boolean criacaoTabelaDestino(MutableBoolean flagCriacao) {

        log.info("Iniciando criação da tabela de destino");

        CreateTableRequest request = new CreateTableRequest()
                .withAttributeDefinitions(new AttributeDefinition(SIGLA_EMISSOR, ScalarAttributeType.S),
                        new AttributeDefinition(CNPJ_CPF, ScalarAttributeType.S))
                .withKeySchema(new KeySchemaElement(SIGLA_EMISSOR, KeyType.HASH),
                        new KeySchemaElement(CNPJ_CPF, KeyType.RANGE))
                .withProvisionedThroughput(new ProvisionedThroughput(10L, 10L))
                .withTableName(destination);

        try {
            CreateTableResult result = amazonDynamoDB.createTable(request);
            log.info("Tabela criada com sucesso: " + result.getTableDescription().getTableName());
            flagCriacao.setTrue();
        }
        catch (AmazonServiceException e) {
            if (!e.getErrorMessage().equals(CANNOT_CREATE_PREEXISTING_TABLE)
                    && !e.getErrorMessage().startsWith(TABLE_ALREADY_EXISTS)) {
                log.error(e.getErrorMessage());
                return false;
            }
            log.info("Tabela já existente.");
        }

        return true;
    }

}
