/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.changer;

/**
 * Interface responsável por ler e modificar a entrada.
 * 
 * @author Verity
 *
 */
public interface ClienteRhChanger {

    boolean copiaDadosTabelaClienteRh();

}
