/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Representa os diversos tipos de endereços que uma pessoa Física ou Jurídica
 * pode ter.
 * 
 * @author Verity
 *
 */
@NoArgsConstructor(force = true)
@EqualsAndHashCode
@ToString
@Data
@JsonPropertyOrder(value = { "cep", "uf", "cidade", "bairro", "tipo", "tipoLogradouro", "logradouro", "numero",
        "complemento" })
@DynamoDBDocument
public class Endereco implements Serializable {

    private static final long serialVersionUID = -8307969569653121979L;

    @DynamoDBAttribute(attributeName = "CEP")
    private String cep;

    @DynamoDBAttribute(attributeName = "UF")
    private String uf;

    @DynamoDBAttribute(attributeName = "Cidade")
    private String cidade;

    @DynamoDBAttribute(attributeName = "Bairro")
    private String bairro;

    @DynamoDBAttribute(attributeName = "Tipo")
    private String tipo;

    @DynamoDBAttribute(attributeName = "tipoLogradouro")
    private String tipoLogradouro;

    @DynamoDBAttribute(attributeName = "Logradouro")
    private String logradouro;

    @DynamoDBAttribute(attributeName = "Numero")
    private Integer numero;

    @DynamoDBAttribute(attributeName = "Complemento")
    private String complemento;

    /**
     * Setter do tipo.
     * 
     * @param tipo
     *            TipoEndereco
     */
    public void setTipo(String tipo) {
        if (StringUtils.isNotBlank(tipo)) {
            this.tipo = tipo.toUpperCase();
        }
    }
}
