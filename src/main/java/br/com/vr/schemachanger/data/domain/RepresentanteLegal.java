/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain;

import java.io.Serializable;
import java.time.LocalDate;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.vr.schemachanger.data.converter.LocalDateTimeConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Representa o contato de uma pessoa (Física ou Jurídica).
 * 
 * @author Verity
 *
 */
@NoArgsConstructor(force = true)
@EqualsAndHashCode
@ToString
@Data
@JsonPropertyOrder(value = {"cpf", "nome", "dataNascimento", "cargo", "rg", "orgaoExpeditor"})
@DynamoDBDocument
public class RepresentanteLegal implements Serializable {

    private static final long serialVersionUID = -6612881562951750054L;

    @DynamoDBAttribute(attributeName = "Cpf")
    private String cpf;

    @DynamoDBAttribute(attributeName = "NomeRepresentante")
    private String nome;

    @DynamoDBTypeConverted(converter = LocalDateTimeConverter.class)
    @DynamoDBAttribute(attributeName = "DataNascimento")
    private LocalDate dataNascimento;

    @DynamoDBAttribute(attributeName = "Cargo")
    private String cargo;

    @DynamoDBAttribute(attributeName = "RG")
    private String rg;

    @DynamoDBAttribute(attributeName = "OrgaoExpedidor")
    private String orgaoExpedidor;

}
