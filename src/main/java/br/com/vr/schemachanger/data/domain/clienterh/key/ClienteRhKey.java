/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain.clienterh.key;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * <p>
 *  .
 * </p>
 * <b>A chave desta entidade deve ser "cnpj" + "siglaEmissor"</b>
 * 
 * @author Verity
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@DynamoDBDocument
public class ClienteRhKey implements Serializable {

    private static final long serialVersionUID = -8340718030641097736L;

    @DynamoDBHashKey(attributeName = "CnpjCpf")
    private String cnpjCpf;

    @DynamoDBRangeKey(attributeName = "SiglaEmissor")
    private String siglaEmissor;

}
