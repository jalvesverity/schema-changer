/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Representa o contato de uma pessoa (Física ou Jurídica).
 * 
 * @author Verity
 *
 */
@NoArgsConstructor(force = true)
@EqualsAndHashCode
@ToString
@Data
@JsonPropertyOrder(value = {"nome", "tipoTelefone", "telefone", "tipoEmail1", "email1", "tipoEmail2", "email2"})
@DynamoDBDocument
public class Contato implements Serializable {

    private static final long serialVersionUID = -4970213917325798179L;

    @DynamoDBAttribute(attributeName = "Nome")
    private String nome;

    @DynamoDBAttribute(attributeName = "TipoTelefone")
    private String tipoTelefone;

    @DynamoDBAttribute(attributeName = "Telefone")
    private String telefone;

    @DynamoDBAttribute(attributeName = "TipoEmail1")
    private String tipoEmail1;

    @DynamoDBAttribute(attributeName = "Email1")
    private String email1;

    @DynamoDBAttribute(attributeName = "TipoEmail2")
    private String tipoEmail2;

    @DynamoDBAttribute(attributeName = "Email2")
    private String email2;

    public void setTipoTelefone(String tipoTelefone) {
        if (StringUtils.isNotBlank(tipoTelefone)) {
            this.tipoTelefone = tipoTelefone.toUpperCase();
        }
    }

    public void setTipoEmail1(String tipoEmail1) {
        if (StringUtils.isNotBlank(tipoEmail1)) {
            this.tipoEmail1 = tipoEmail1.toUpperCase();
        }
    }

    public void setTipoEmail2(String tipoEmail2) {
        if (StringUtils.isNotBlank(tipoEmail2)) {
            this.tipoEmail2 = tipoEmail2.toUpperCase();
        }
    }
}
