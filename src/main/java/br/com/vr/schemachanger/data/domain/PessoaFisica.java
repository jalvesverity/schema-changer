/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain;

import java.io.Serializable;
import java.time.LocalDate;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.vr.schemachanger.data.converter.LocalDateTimeConverter;
import lombok.Data;

/**
 * Representa as informações que identificam uma Pessoa Física.
 * 
 * @author Verity
 *
 */
@Data
@JsonPropertyOrder(value = {"rg", "orgaoExpeditor", "nome", "sexo", "dataNascimento", "nomeMae", "escolaridade"})
@DynamoDBDocument
public class PessoaFisica implements Serializable {


    private static final long serialVersionUID = -3043353237010080373L;

    @DynamoDBAttribute(attributeName = "Rg")
    private String rg;

    @DynamoDBAttribute(attributeName = "OrgaoExpedidor")
    private String orgaoExpedidor;

    @DynamoDBAttribute(attributeName = "NomePessoaFisica")
    private String nome;

    @DynamoDBAttribute(attributeName = "Sexo")
    private String sexo;

    @DynamoDBTypeConverted(converter = LocalDateTimeConverter.class)
    @DynamoDBAttribute(attributeName = "DataNascimento")
    private LocalDate dataNascimento;

    @DynamoDBAttribute(attributeName = "NomeMaePessoaFisica")
    private String nomeMae;

    @DynamoDBAttribute(attributeName = "Escolaridade")
    private String escolaridade;

}
