/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain.enums;

/**
 * A classificação do EC para o Emissor. Esse campo não deverá ser retornado nem
 * recebido de nenhuma interface. Será apresentado read-only no CRM.
 * 
 * @author Verity
 * 
 */
public enum ClassificacaoQuantitativaEC {

    /**
     * CORPORATE.
     */
    CORPORATE("C"),

    /**
     * MIDDLE.
     */
    MIDDLE("M"),

    /**
     * VAREJO.
     */
    VAREJO("V");

    private final String value;

    /**
     * Cria um novo valor.
     * 
     * @param value String com o valor que deseja retornar.
     * 
     */
    ClassificacaoQuantitativaEC(String value) {
        this.value = value;
    }

    /**
     * Recupera o valor.
     * 
     * @return String com o valor a ser verificado.
     */
    public String value() {
        return this.value;
    }

}
