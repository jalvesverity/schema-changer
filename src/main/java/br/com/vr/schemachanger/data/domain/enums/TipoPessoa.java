/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain.enums;

/**
 * Indica a situação fiscal da pessoa na receita federal.
 * 
 * @author Verity
 * 
 */
public enum TipoPessoa {
    
    /**
     * Se a quantidade de caracteres informados for 11 então este campo deve ser
     * preenchido automaticamente com "FISICA".
     */
    FISICA("FISICA"),

    /**
     * Se a quantidade de caracteres informados for 14 então este campo deve ser
     * preenchido automaticamente com "JURIDICA".
     */
    JURIDICA("JURIDICA");

    private final String value;

    /**
     * Cria um novo valor.
     * 
     * @param value String com o valor que deseja retornar.
     * 
     */
    TipoPessoa(String value) {
        this.value = value;
    }

    /**
     * Recupera o valor.
     * 
     * @return String com o valor a ser verificado.
     */
    public String value() {
        return this.value;
    }

}
