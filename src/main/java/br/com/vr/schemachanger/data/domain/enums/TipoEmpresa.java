/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain.enums;

/**
 * efine o tipo de empresa da pessoa jurídica.
 * 
 * @author Verity
 * 
 */
public enum TipoEmpresa {
    /**
     * MATRIZ.
     */
    MATRIZ("MATRIZ"),

    /**
     * FILIAL.
     */
    FILIAL("FILIAL");

    private final String value;

    /**
     * Cria um novo valor.
     * 
     * @param value
     *            String com o valor que deseja retornar.
     * 
     */
    TipoEmpresa(String value) {
        this.value = value;
    }

    /**
     * Recupera o valor.
     * 
     * @return String com o valor a ser verificado.
     */
    public String value() {
        return this.value;
    }

}
