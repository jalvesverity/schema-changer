/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain.clienterh;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.vr.schemachanger.data.domain.clienterh.enums.TipoDocumento;
import br.com.vr.schemachanger.data.domain.clienterh.key.ClienteRhKey;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Entidade ClienteRH.
 * A chave desta entidade deve ser "cnpj" + "siglaEmissor"
 * @author Verity
 */
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Data
@JsonPropertyOrder(value = { "siglaEmissor", "cnpjCpf", "tipoDocumento", "cnpjGrupo", "nomeGrupo", 
        "idConfiguracaoCobranca", "emailAlertaPedido", "emailAlertaFinanceiro", "nomeImpressao", 
        "flagEmiteCobranca", "flagEmiteNotaPorProduto", "flagEmiteNotaSemServico", "flagFaturaPrePagoAntecipado",
        "segregacaoCobrancaPrePago", "segregacaoCobrancaPosPago", "pessoa", "clientesAssociadosFaturamento",
        "atendimentosClienteRh", "produtosContratados" })
@DynamoDBTable(tableName = "ClienteRh.ClienteRh.ClienteRh")
public class ClienteRh extends AbstractDocument implements Serializable {

    private static final long serialVersionUID = 1L;

    private static final int VL_TAMANHO_CNPJ = 14;

    @Id
    @JsonIgnore
    @DynamoDBIgnore
    private ClienteRhKey clienteRhKey;

    @DynamoDBAttribute(attributeName = "TipoDocumento")
    private String tipoDocumento;

    @DynamoDBAttribute(attributeName = "CnpjGrupo")
    private String cnpjGrupo;

    @DynamoDBAttribute(attributeName = "NomeGrupo")
    private String nomeGrupo;

    @DynamoDBAttribute(attributeName = "IdConfiguracaoCobranca")
    private Long idConfiguracaoCobranca;

    @DynamoDBAttribute(attributeName = "EmailAlertaPedido")
    private String emailAlertaPedido;

    @DynamoDBAttribute(attributeName = "EmailAlertaFinanceiro")
    private String emailAlertaFinanceiro;

    @DynamoDBAttribute(attributeName = "NomeImpressao")
    private String nomeImpressao; 

    @DynamoDBAttribute(attributeName = "FlagEmiteCobranca")
    private Boolean flagEmiteCobranca;

    @DynamoDBAttribute(attributeName = "FlagEmiteNotaPorProduto")
    private Boolean flagEmiteNotaPorProduto;

    @DynamoDBAttribute(attributeName = "FlagEmiteNotaSemServico")
    private Boolean flagEmiteNotaSemServico;

    @DynamoDBAttribute(attributeName = "FlagFaturaPrePagoAntecipado")
    private Boolean flagFaturaPrePagoAntecipado;

    @DynamoDBAttribute(attributeName = "SegregacaoCobrancaPrePago")
    private String segregacaoCobrancaPrePago;

    @DynamoDBAttribute(attributeName = "SegregacaoCobrancaPosPago")
    private String segregacaoCobrancaPosPago;

    @DynamoDBAttribute(attributeName = "Pessoa")
    private Pessoa pessoa;

    @DynamoDBAttribute(attributeName = "ClientesAssociadosFaturamento")
    private List<ClienteAssociadoFaturamento> clientesAssociadosFaturamento;

    @DynamoDBAttribute(attributeName = "AtendimentosClienteRh")
    private List<AtendimentoClienteRh> atendimentosClienteRh;

    @DynamoDBAttribute(attributeName = "ProdutosContratados")
    private List<ProdutoContratado> produtosContratados;

    @DynamoDBAttribute(attributeName = "RazaoSocial")
    private String razaoSocial;

    @DynamoDBAttribute(attributeName = "NomeFantasia")
    private String nomeFantasia;
    
    @DynamoDBAttribute(attributeName = "CnpjCpfAssociadosFaturamento")
    private String cnpjCpfAssociadosFaturamento;

    @DynamoDBHashKey(attributeName = "CnpjCpf")
    public String getCnpjCpf() {
        if (Objects.nonNull(clienteRhKey)) {
            return clienteRhKey.getCnpjCpf();
        }
        return null;
    }

    /**
     * Método SET umplementado para o CNPJ do Cliente RH.
     * 
     * @param cnpj
     *          String - Corresponde ao CNPJ do Cliente RH.
     * 
     */
    public void setCnpjCpf(final String cnpj) {
        if (Objects.isNull(clienteRhKey)) {
            clienteRhKey = new ClienteRhKey();
        }
        clienteRhKey.setCnpjCpf(StringUtils.upperCase(StringUtils.trimToEmpty(cnpj)));
        this.tipoDocumento = this.definirTipoDocumento(cnpj).value();
    }

    @DynamoDBRangeKey(attributeName = "SiglaEmissor")
    public String getSiglaEmissor() {
        if (Objects.nonNull(clienteRhKey)) {
            return clienteRhKey.getSiglaEmissor();
        }
        return null;
    }

    /**
     * Método SET umplementado para a sigla do emissor da chave composta do cliente RH.
     * 
     * @param siglaEmissor
     *          String - sigla do emissor que o cliente RH pertence
     * 
     */
    public void setSiglaEmissor(final String siglaEmissor) {
        if (Objects.isNull(clienteRhKey)) {
            clienteRhKey = new ClienteRhKey();
        }
        clienteRhKey.setSiglaEmissor(StringUtils.upperCase(StringUtils.trimToEmpty(siglaEmissor)));
    }

    /**
     * Funcao para definir o tipo do documento atraves do cnpjCpf informado.
     */
    private TipoDocumento definirTipoDocumento(final String cnpjCpf) {
        if (StringUtils.length(cnpjCpf) == VL_TAMANHO_CNPJ) {
            return TipoDocumento.CNPJ;
        } 
        else {
            return TipoDocumento.CPF;
        }
    }

}
