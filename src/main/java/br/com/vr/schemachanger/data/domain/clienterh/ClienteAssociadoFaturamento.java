/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain.clienterh;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.vr.schemachanger.data.domain.clienterh.enums.TipoDocumento;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Entidade ClienteAssociadoFaturamento.
 * Representa os RH´s associados ao cliente RH pai, para faturamento.
 * @author Verity
 */
@EqualsAndHashCode
@Data
@JsonPropertyOrder(value = { "cnpjCpf", "tipoDocumento", "razaoSocial" })
@DynamoDBDocument
public class ClienteAssociadoFaturamento implements Serializable {

    private static final long serialVersionUID = 8245041378466302087L;

    private static final int VL_TAMANHO_CNPJ = 14;

    @DynamoDBAttribute(attributeName = "CnpjCpf")
    private String cnpjCpf;

    @DynamoDBAttribute(attributeName = "TipoDocumento")
    private String tipoDocumento;

    @DynamoDBAttribute(attributeName = "RazaoSocial")
    private String razaoSocial;

    public void setCnpjCpf(String cnpjCpf) {
        this.cnpjCpf = StringUtils.upperCase(StringUtils.trimToEmpty(cnpjCpf));
        this.tipoDocumento = this.definirTipoDocumento(cnpjCpf).value();
    }

    /**
     * Funcao para definir o tipo do documento atraves do cnpjCpf informado.
     */
    private TipoDocumento definirTipoDocumento(final String cnpjCpf) {
        if (StringUtils.length(cnpjCpf) == VL_TAMANHO_CNPJ) {
            return TipoDocumento.CNPJ;
        } 
        else {
            return TipoDocumento.CPF;
        }
    }

}
