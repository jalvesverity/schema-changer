/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.annotation.Id;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBIgnore;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.vr.schemachanger.data.domain.enums.TipoPessoa;
import br.com.vr.schemachanger.data.domain.key.PessoaKey;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Classe representando nova entrada.
 * 
 * @author Verity
 *
 */
@NoArgsConstructor(force = true)
@EqualsAndHashCode(callSuper = false)
@ToString
@Data
@JsonPropertyOrder(value = {"siglaEmissor", "cnpjCpf", "tipoPessoa", "situacaoFiscal", "pessoaFisica", 
        "pessoaJuridica", "enderecos", "contatos", "naturezasJuridicas"})
@DynamoDBTable(tableName = "Pessoa.Pessoa.Pessoa2")
public class NovaPessoa extends AbstractDocument implements Serializable {

    private static final long serialVersionUID = -5074105786646873508L;

    private static final Integer CNPJ_TAM = 14;

    /**
     * Objeto representativo da chave.
     * 
     * @see PessoaKey
     */
    @Id
    @JsonIgnore
    @DynamoDBIgnore
    private PessoaKey pessoaKey;

    @DynamoDBAttribute(attributeName = "TipoPessoa")
    private String tipoPessoa;

    @DynamoDBAttribute(attributeName = "SituacaoFiscal")
    private String situacaoFiscal;

    @DynamoDBAttribute(attributeName = "PessoaFisica")
    private PessoaFisica pessoaFisica;

    @DynamoDBAttribute(attributeName = "PessoaJuridica")
    private PessoaJuridica pessoaJuridica;

    @DynamoDBAttribute(attributeName = "Enderecos")
    private List<NovoEndereco> enderecos;

    @DynamoDBAttribute(attributeName = "Contatos")
    private List<Contato> contatos;

    @DynamoDBAttribute(attributeName = "NaturezasJuridicas")
    private List<NaturezaJuridicaPessoa> naturezasJuridicas;

    @DynamoDBAttribute(attributeName = "BuscaNomePessoaFisica")
    private String buscaNomePessoaFisica;

    @DynamoDBAttribute(attributeName = "BuscaNomeFantasia")
    private String buscaNomeFantasia;

    @DynamoDBAttribute(attributeName = "BuscaRazaoSocial")
    private String buscaRazaoSocial;

    @DynamoDBHashKey(attributeName = "SiglaEmissor")
    public String getSiglaEmissor() {
        if (Objects.nonNull(this.pessoaKey)) {
            return this.pessoaKey.getSiglaEmissor();
        }

        return null;
    }

    /**
     * Método implementado para setar a sigla emissor.
     * 
     * @param siglaEmissor
     *          String
     */
    public void setSiglaEmissor(final String siglaEmissor) {
        if (Objects.isNull(this.pessoaKey)) {
            this.pessoaKey = new PessoaKey();
        }

        this.pessoaKey.setSiglaEmissor(
                StringUtils.upperCase(StringUtils.trimToEmpty(siglaEmissor)));
    }

    @DynamoDBRangeKey(attributeName = "CnpjCpf")
    public String getCnpjCpf() {
        if (Objects.nonNull(this.pessoaKey)) {
            return this.pessoaKey.getCnpjCpf();
        }

        return null;
    }

    /**
     * Método implementado para setar o cpf/cnpj e o tipoPessoa.
     * 
     * @param cnpjCpf
     *          String
     */
    public void setCnpjCpf(final String cnpjCpf) {
        if (Objects.isNull(this.pessoaKey)) {
            this.pessoaKey = new PessoaKey();
        }

        this.pessoaKey.setCnpjCpf(
                StringUtils.upperCase(StringUtils.trimToEmpty(cnpjCpf)));

        this.tipoPessoa = this.defineTipoPessoa(cnpjCpf).value();
    }

    public void setBuscaNomePessoaFisica(final String nomePessoaFisica) {
        this.buscaNomePessoaFisica = StringUtils.stripAccents(nomePessoaFisica.toUpperCase());
    }

    public void setBuscaNomeFantasia(final String nomeFantasia) {
        this.buscaNomeFantasia = StringUtils.stripAccents(nomeFantasia.toUpperCase());
    }

    public void setBuscaRazaoSocial(final String razaoSocial) {
        this.buscaRazaoSocial = StringUtils.stripAccents(razaoSocial.toUpperCase());
    }

    /**
     * Método para definição do tipoPessoa.
     * 
     * @param cnpjCpf
     *          String - cnpj ou cpf da pessoa.
     * 
     * @return TipoPessoa
     * 
     * @see TipoPessoa
     */
    private TipoPessoa defineTipoPessoa(String cnpjCpf) {
        if (StringUtils.length(cnpjCpf) == CNPJ_TAM) {
            return TipoPessoa.JURIDICA;
        }
        else {
            return TipoPessoa.FISICA;
        }
    }
}
