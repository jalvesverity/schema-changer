/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Entidade responsável por armazenar o cadastro da natureza jurídica de uma
 * empresa definido pela Receita Federal.
 * 
 * @author Verity
 *
 */
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Data
@JsonPropertyOrder(value = {"codigo", "descricao"})
@DynamoDBDocument
public class NaturezaJuridicaPessoa implements Serializable {

    private static final long serialVersionUID = 1764923382447392471L;

    @DynamoDBAttribute(attributeName = "Codigo")
    private String codigo;

    private String descricao;

}
