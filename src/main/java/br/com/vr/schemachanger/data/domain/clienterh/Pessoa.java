/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain.clienterh;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Entidade Pessoa.
 * Representa uma pessoa jurídica no sistema. A mesma pode ou não ter um CNPJ.
 * 
 * @author Verity
 */
@EqualsAndHashCode
@Data
@JsonPropertyOrder(value = { "razaoSocial", "nomeFantasia", "inscricaoMunicipal", "inscricaoEstadual" })
@DynamoDBDocument
public class Pessoa implements Serializable {

    private static final long serialVersionUID = 6748269696616222734L;

    @DynamoDBAttribute(attributeName = "RazaoSocial")
    private String razaoSocial;

    @DynamoDBAttribute(attributeName = "NomeFantasia")
    private String nomeFantasia;

    @DynamoDBAttribute(attributeName = "InscricaoMunicipal")
    private String inscricaoMunicipal;

    @DynamoDBAttribute(attributeName = "InscricaoEstadual")
    private String inscricaoEstadual;

    @DynamoDBAttribute(attributeName = "Endereco")
    private Endereco endereco;

}
