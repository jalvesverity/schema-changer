/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.config;

import org.socialsignin.spring.data.dynamodb.core.DynamoDBOperations;
import org.socialsignin.spring.data.dynamodb.core.DynamoDBTemplate;
import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig.TableNameOverride;

import lombok.extern.slf4j.Slf4j;

/**
 * Classe responsável para obter configurações de banco de dados
 * para o ambiente de desenvolvimento.
 * 
 * @author Verity
 */
@Slf4j
@Configuration
@EnableDynamoDBRepositories(basePackages = "br.com.vr.schemachanger.data.repository",
                            dynamoDBMapperConfigRef = "dynamoDbMapperConfig")
public class DynamoDbConfigImpl implements DynamoDbConfig {

    @Value("${amazon.dynamodb.endpoint}")
    private String amazonDynamoDbEndpoint;

    @Value("${amazon.aws.accesskey}")
    private String amazonAwsAccessKey;

    @Value("${amazon.aws.secretkey}")
    private String amazonAwsSecretKey;

    @Value("${amazon.dynamodb.table-prefix}")
    private String tablePrefix;

    /**
     * {@inheritDoc}
     */
    @Bean
    @Override
    public AmazonDynamoDB amazonDynamoDB(AWSCredentialsProvider amazonAwsCredentials) {

        final String signingRegionLocal = "local";

        final AmazonDynamoDBClientBuilder clientBuilder = AmazonDynamoDBClientBuilder.standard()
                                                                         .withCredentials(this.amazonAwsCredentials());

        if (!StringUtils.isEmpty(this.amazonDynamoDbEndpoint)) {

            log.debug("Iniciando Dynamodb local no caminho {}", this.amazonDynamoDbEndpoint);

            clientBuilder.withEndpointConfiguration(
                    new AwsClientBuilder.EndpointConfiguration(this.amazonDynamoDbEndpoint, signingRegionLocal));
        }
        else {
            clientBuilder.withRegion(Regions.SA_EAST_1);

            log.debug("Iniciando Dynamodb na regiao {}", clientBuilder.getRegion());
        }

        return clientBuilder.build();
    }


    /**
     * {@inheritDoc}
     */
    @Bean
    @Override
    public AWSCredentialsProvider amazonAwsCredentials() {
        return new AWSStaticCredentialsProvider(
                new BasicAWSCredentials(this.amazonAwsAccessKey, this.amazonAwsSecretKey));
    }

    /**
     * {@inheritDoc}
     */
    @Bean
    @Override
    public DynamoDBOperations dynamoDbOperations() {
        return new DynamoDBTemplate(this.amazonDynamoDB(this.amazonAwsCredentials()), this.dynamoDbMapperConfig());
    }

    /**
     * {@inheritDoc}
     */
    @Bean
    @Override
    public DynamoDBMapper dynamoDbMapper() {
        return new DynamoDBMapper(this.amazonDynamoDB(this.amazonAwsCredentials()), this.dynamoDbMapperConfig());
    }

    /**
     * {@inheritDoc}
     */
    @Bean
    @Override
    public DynamoDBMapperConfig dynamoDbMapperConfig() {
        return new DynamoDBMapperConfig.Builder().withTableNameOverride(
                TableNameOverride.withTableNamePrefix(StringUtils.trimAllWhitespace(this.tablePrefix))).build();
    }
}
