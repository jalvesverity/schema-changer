/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain.clienterh;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode
@Data
@JsonPropertyOrder(value = { "cep", "uf", "cidade", "bairro", "tipo", "tipoLogradouro",
        "logradouro", "numero", "complemento" })
@DynamoDBDocument
public class NovoEndereco implements Serializable {

    private static final long serialVersionUID = -3328374850481622868L;

    @DynamoDBAttribute(attributeName = "Cep")
    private String cep;

    @DynamoDBAttribute(attributeName = "Uf")
    private String uf;

    @DynamoDBAttribute(attributeName = "Cidade")
    private String cidade;

    @DynamoDBAttribute(attributeName = "Bairro")
    private String bairro;

    @DynamoDBAttribute(attributeName = "Tipo")
    private String tipo;

    @DynamoDBAttribute(attributeName = "TipoLogradouro")
    private String tipoLogradouro;

    @DynamoDBAttribute(attributeName = "Logradouro")
    private String logradouro;

    @DynamoDBAttribute(attributeName = "Numero")
    private String numero;

    @DynamoDBAttribute(attributeName = "Complemento")
    private String complemento;

}
