/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Representa a identificação jurídico-institucional das entidades públicas e
 * privadas. A natureza jurídica está agregada em cinco grupos: administração
 * pública, entidades empresariais, entidades sem fins lucrativos, Pessoas
 * Físicas e Organizações Internacionais e Outras Instituições
 * Extraterritoriais.
 * 
 * @author Verity
 *
 */
@NoArgsConstructor(force = true)
@EqualsAndHashCode
@ToString
@Data
@JsonPropertyOrder(value = {"codigo", "descricao"})
@DynamoDBTable(tableName = "Pessoa.Pessoa.NaturezaJuridica")
public class NaturezaJuridica {

    @DynamoDBHashKey(attributeName = "Codigo")
    private String codigo;

    @DynamoDBAttribute(attributeName = "Descricao")
    private String descricao;

    @DynamoDBAttribute(attributeName = "Descricao-BUSCA")
    private String descricaoBusca;

}
