/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain.clienterh;

import java.io.Serializable;
import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.vr.schemachanger.data.converter.LocalDateTimeConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Entidade AtendimentoClienteRH.
 * Representa o canal/subcanal  vigente que está prestando atendimento ao cliente RH.
 * 
 * @author Verity
 */
@EqualsAndHashCode
@Data
@JsonPropertyOrder(value = { "siglaCanalAtendimento", "idSubcanalAtendimento", "statusAtendimento",
        "dataInicioAtendimento", "dataFimAtendimento" })
@DynamoDBDocument
public class AtendimentoClienteRh implements Serializable {

    private static final long serialVersionUID = 5121388744617075378L;

    @DynamoDBAttribute(attributeName = "SiglaCanalAtendimento")
    private String siglaCanalAtendimento;

    @DynamoDBAttribute(attributeName = "IdSubcanalAtendimento")
    private Long idSubcanalAtendimento;

    @DynamoDBAttribute(attributeName = "StatusAtendimento")
    private String statusAtendimento;

    @DynamoDBAttribute(attributeName = "DataInicioAtendimento")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @DynamoDBTypeConverted(converter = LocalDateTimeConverter.class)
    private LocalDate dataInicioAtendimento;

    @DynamoDBAttribute(attributeName = "DataFimAtendimento")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @DynamoDBTypeConverted(converter = LocalDateTimeConverter.class)
    private LocalDate dataFimAtendimento;

}
