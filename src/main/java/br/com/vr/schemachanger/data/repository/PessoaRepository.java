/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.repository;

import java.util.List;

import org.socialsignin.spring.data.dynamodb.repository.DynamoDBCrudRepository;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;

import br.com.vr.schemachanger.data.domain.Pessoa;
import br.com.vr.schemachanger.data.domain.key.PessoaKey;

/**
 * Interface contendo as chamadas dos métodos de busca utilizados no microsserviço Pessoa.
 * 
 * @author verity
 */
@EnableScan
public interface PessoaRepository extends DynamoDBCrudRepository<Pessoa, PessoaKey> {

    List<Pessoa> findAll();
}
