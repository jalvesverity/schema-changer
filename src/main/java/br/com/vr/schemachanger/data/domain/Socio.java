/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain;

import java.io.Serializable;
import java.time.LocalDate;

import org.apache.commons.lang3.StringUtils;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;

import br.com.vr.schemachanger.data.converter.LocalDateTimeConverter;
import br.com.vr.schemachanger.data.domain.enums.TipoPessoa;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Representa um sócio da PJ registrado no contrato social da mesma.
 * 
 * @author Verity
 *
 */
@NoArgsConstructor(force = true)
@EqualsAndHashCode
@ToString
@Data
@DynamoDBDocument
public class Socio implements Serializable {

    private static final long serialVersionUID = 8887140546324960543L;

    private static final Integer CNPJ_TAM = 14;

    @DynamoDBAttribute(attributeName = "CnpjCpf")
    private String cnpjCpf;

    @DynamoDBAttribute(attributeName = "Nome")
    private String nome;

    @DynamoDBAttribute(attributeName = "TipoPessoa")
    private String tipoPessoa;

    @DynamoDBTypeConverted(converter = LocalDateTimeConverter.class)
    @DynamoDBAttribute(attributeName = "DataEntradaSociedade")
    private LocalDate dataEntradaSociedade;

    
    /**
     * Construtor para Socio, com os campos, sem TipoPessoa.
     * 
     * @param cnpjCpf
     *            String - cnpj ou cpf da pessoa.
     * @param nome
     *            String - nome da pessoa
     * @param dataEntradaSociedade
     *            - LocalDate dataEntradaSociedade
     */
    public Socio(String cnpjCpf, String nome, LocalDate dataEntradaSociedade) {
        this.setCnpjCpf(cnpjCpf);
        this.setNome(nome);
        this.dataEntradaSociedade = dataEntradaSociedade;
    }
    
    /**
     * Método implementado para setar o cpf/cnpj e o tipoPessoa.
     * 
     * @param cnpjCpf
     *          String
     */
    public void setCnpjCpf(final String cnpjCpf) {
        this.cnpjCpf = StringUtils.upperCase(StringUtils.trimToEmpty(cnpjCpf));
        this.tipoPessoa = this.defineTipoPessoa(cnpjCpf).value();
    }

    /**
     * Método para definição do tipoPessoa.
     * 
     * @param cnpjCpf
     *          String - cnpj ou cpf da pessoa.
     * 
     * @return TipoPessoa
     * 
     * @see TipoPessoa
     */
    private TipoPessoa defineTipoPessoa(String cnpjCpf) {
        if (StringUtils.length(cnpjCpf) == CNPJ_TAM) {
            return TipoPessoa.JURIDICA;
        }
        else {
            return TipoPessoa.FISICA;
        }
    }

}
