package br.com.vr.schemachanger.data.domain.clienterh;

import java.io.Serializable;
import java.time.LocalDate;

import org.springframework.format.annotation.DateTimeFormat;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;

import br.com.vr.schemachanger.data.converter.LocalDateTimeConverter;

public class NovoAtendimentoClienteRh implements Serializable {

    private static final long serialVersionUID = 5121388744617075378L;

    @DynamoDBAttribute(attributeName = "SiglaCanalAtendimento")
    private String siglaCanalAtendimento;

    @DynamoDBAttribute(attributeName = "IdSubcanalAtendimento")
    private Long idSubcanalAtendimento;

    @DynamoDBAttribute(attributeName = "StatusNovoAtendimento")
    private String statusNovoAtendimento;

    @DynamoDBAttribute(attributeName = "DataInicioAtendimento")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @DynamoDBTypeConverted(converter = LocalDateTimeConverter.class)
    private LocalDate dataInicioAtendimento;

    @DynamoDBAttribute(attributeName = "DataFimAtendimento")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @DynamoDBTypeConverted(converter = LocalDateTimeConverter.class)
    private LocalDate dataFimAtendimento;

}
