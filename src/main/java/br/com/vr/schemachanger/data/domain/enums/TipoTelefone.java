/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain.enums;

/**
 * O tipo de telefone do contato do CNPJ no cadastro do CRM.
 * 
 * @author Verity
 * 
 */
public enum TipoTelefone {

    /**
     * EMPRESA.
     */
    EMPRESA("EMPRESA"),

    /**
     * CELULAR.
     */
    CELULAR("CELULAR"),

    /**
     * FAX.
     */
    FAX("FAX"),

    /**
     * RESIDENCIA.
     */
    RESIDENCIA("RESIDENCIA"),

    /**
     * CAMPUS.
     */
    CAMPUS("CAMPUS"),

    /**
     * OUTRO.
     */
    OUTRO("OUTRO"),

    /**
     * PRINCIPAL.
     */
    PRINCIPAL("PRINCIPAL");

    private final String value;

    /**
     * Cria um novo valor.
     * 
     * @param value
     *            String com o valor que deseja retornar.
     * 
     */
    TipoTelefone(String value) {
        this.value = value;
    }

    /**
     * Recupera o valor.
     * 
     * @return String com o valor a ser verificado.
     */
    public String value() {
        return this.value;
    }

}
