/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain.enums;

/**
 * O tipo de e-mail do contato do CNPJ no cadastro do CRM.
 */
public enum TipoEmail {

    /**
     * Enum - Empresa (27).
     */
    EMPRESA("EMPRESA"),

    /**
     * Enum - Dormitório (29).
     */
    DORMITORIO("DORMITORIO"),

    /**
     * Enum - Residência (30).
     */
    RESIDENCIA("RESIDENCIA"),

    /**
     * Enum - Outro (30).
     */
    OUTRO("OUTRO"),

    /**
     * Enum - Campus (28).
     */
    CAMPUS("CAMPUS");

    private final String value;

    /**
     * Construtor TipoEmail.
     * @param value
     *          Valor do TipoEmail.
     */
    TipoEmail(String value) {
        this.value = value;
    }

    /**
     * Retorna o valor do TipoEmail.
     * @return String
     */
    public String value() {
        return this.value;
    }

}
