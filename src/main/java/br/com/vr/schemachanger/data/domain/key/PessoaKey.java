/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain.key;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Objeto Chave Pessoa. 
 * <br>
 * A chave desta entidade deve ser siglaEmissor + cnpjCpf
 * 
 * @author Verity
 */
@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class PessoaKey implements Serializable {

    private static final long serialVersionUID = 9104908787919724202L;

    /**
     * Define um identificador único que representa a sigla do Emissor nos sistemas
     * da VR.
     */
    @DynamoDBHashKey(attributeName = "SiglaEmissor")
    private String siglaEmissor;

    /**
     * Identifica o número do documento que pode ser uma pessoa jurídica (CNPJ) ou
     * uma pessoa fisíca(CPF). Este campo deverá suportar somente uma informação ou
     * CPF ou CNPJ.
     */
    @DynamoDBRangeKey(attributeName = "CnpjCpf")
    private String cnpjCpf;

}
