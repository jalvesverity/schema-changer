/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain.enums;

/**
 * Tipo do endereço cadastrado.
 * 
 * @author Verity
 * 
 */
public enum TipoEndereco {

    /**
     * CAMPUS.
     */
    CAMPUS("CAMPUS"),

    /**
     * COMERCIAL.
     */
    COMERCIAL("COMERCIAL"),

    /**
     * CORRESPONDENCIA.
     */
    CORRESPONDENCIA("CORRESPONDENCIA"),

    /**
     * FISCAL.
     */
    FISCAL("FISCAL"),

    /**
     * INSTALACAO.
     */
    INSTALACAO("INSTALACAO"),

    /**
     * OUTRO.
     */
    OUTRO("OUTRO"),

    /**
     * REPRESENTANTE_LEGAL.
     */
    REPRESENTANTE_LEGAL("REPRESENTANTE_LEGAL"),

    /**
     * RESIDENCIA.
     */
    RESIDENCIA("RESIDENCIA"),

    /**
     * DORMITORIO.
     */
    DORMITORIO("DORMITORIO");

    private final String value;

    /**
     * Cria um novo valor.
     * 
     * @param value
     *            String com o valor que deseja retornar.
     * 
     */
    TipoEndereco(String value) {
        this.value = value;
    }

    /**
     * Recupera o valor.
     * 
     * @return String com o valor a ser verificado.
     */
    public String value() {
        return this.value;
    }

}
