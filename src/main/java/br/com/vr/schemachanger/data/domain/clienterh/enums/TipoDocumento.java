/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain.clienterh.enums;

public enum TipoDocumento {

    /**
     * CPF: CPF.
     */
    CPF("CPF"),

    /**
     * CNPJ: CNPJ.
     */
    CNPJ("CNPJ");

    private final String value;

    /**
     * Cria um novo TipoDocumento.
     * 
     * @param value
     * 
     */
    TipoDocumento(String value) {
        this.value = value;
    }

    /**
     * Recupera o valor do TipoDocumento.
     * 
     * @return String
     * 
     */
    public String value() {
        return this.value;
    }

}
