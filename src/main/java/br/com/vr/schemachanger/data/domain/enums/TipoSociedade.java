/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain.enums;

/**
 * Tipo de sociedade da empresa.
 * 
 * @author Verity
 * 
 */
public enum TipoSociedade {
    
    /**
     * Sociedade Anônima
     * ANONIMA.
     */
    ANONIMA("ANONIMA"),

    /**
     * Sociedade Limitada
     * LIMITADA.
     */
    LIMITADA("LIMITADA"),

    /**
     * Firma Individual
     * FIRMA_INDIVIDUAL.
     */
    FIRMA_INDIVIDUAL("FIRMA_INDIVIDUAL"),

    /**
     * Não informado
     * NAO_INFORMADO.
     */
    NAO_INFORMADO("NAO_INFORMADO"),

    /**
     * Microempresa
     * MICROEMPRESA.
     */
    MICROEMPRESA("MICROEMPRESA"),

    /**
     * MICROEMPREENDEDOR.
     */
    MICROEMPREENDEDOR("MICROEMPREENDEDOR");

    private final String value;

    /**
     * Cria um novo valor.
     * 
     * @param value
     *            String com o valor que deseja retornar.
     * 
     */
    TipoSociedade(String value) {
        this.value = value;
    }

    /**
     * Recupera o valor.
     * 
     * @return String com o valor a ser verificado.
     */
    public String value() {
        return this.value;
    }

}
