/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.config;

import org.socialsignin.spring.data.dynamodb.core.DynamoDBOperations;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;

/**
 * Esta interface é responsável por definir um contrato para qualquer
 * implementação que deseja realizar configurações do DynamoDB.
 * 
 * @author Verity
 */
public interface DynamoDbConfig {

    /**
     * * Método utilizado para acessar o DynamoDB. O Amazon DynamoDB é um serviço de
     * banco de dados NoSQL
     * 
     * @param amazonAwsCredentials
     *            Recebe como parametro as credenciais da amazon AWS
     * @return AmazonDynamoDB
     */
    AmazonDynamoDB amazonDynamoDB(AWSCredentialsProvider amazonAwsCredentials);

    /**
     * Método que fornece acesso às credenciais da AWS utsadas para acessar serviços
     * AWS. ID da chave de acesso da AWS e chave de acesso. Estas credenciais são
     * usadas para garantir a segurança de comunucação com os serviços da AWS
     *
     * @return AWSCredentials
     *
     */
    AWSCredentialsProvider amazonAwsCredentials();

    /**
    * Método que fornece uma classe DynamoDBOperations.
    * 
    * @return DynamoDBOperations
    */
    DynamoDBOperations dynamoDbOperations();

    /**
     * Método que fornece uma classe DynamoDBMapper, permitindo mapear classes no
     * lado do cliente para tabelas do DynamoDB.
     * 
     * @return DynamoDBMapper
     */
    DynamoDBMapper dynamoDbMapper();

    /**
     * Método que fornece configurações personalizadas para um
     * {@link DynamoDBMapper}.
     * 
     * @return DynamoDBMapperConfig
     * @see com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper#DynamoDBMapper(AmazonDynamoDB,
     *      DynamoDBMapperConfig)
     */
    DynamoDBMapperConfig dynamoDbMapperConfig();
}
