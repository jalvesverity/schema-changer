/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain;

import java.io.Serializable;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Representa as atividades que uma empresa pode exercer, servem como sistema de
 * linguagem na produção de informações estatísticas.
 * 
 * @author Verity
 *
 */
@EqualsAndHashCode
@ToString
@Data
@JsonPropertyOrder(value = {"codigo", "descricao", "flagAtividadePrincipal"})
@AllArgsConstructor
@NoArgsConstructor
@DynamoDBDocument
public class AtividadeEconomicaPj implements Serializable {

    private static final long serialVersionUID = 6729463352836875785L;

    @DynamoDBAttribute(attributeName = "CodigoAtividadeEconomica")
    private String codigo;

    private String descricao;

    @DynamoDBAttribute(attributeName = "FlagAtividadePrincipal")
    private Boolean flagAtividadePrincipal;

}
