/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain.enums;

/**
 * Nível de escolaridade da pessoa física.
 * 
 * @author Verity
 * 
 */
public enum Escolaridade {

    /**
     * FUNDAMENTAL_INCOMPLETO.
     */
    FUNDAMENTAL_INCOMPLETO("FUNDAMENTAL_INCOMPLETO"),

    /**
     * FUNDAMENTAL_COMPLETO.
     */
    FUNDAMENTAL_COMPLETO("FUNDAMENTAL_COMPLETO"),

    /**
     * MEDIO_INCOMPLETO.
     */
    MEDIO_INCOMPLETO("MEDIO_INCOMPLETO"),

    /**
     * MEDIO_COMPLETO.
     */
    MEDIO_COMPLETO("MEDIO_COMPLETO"),

    /**
     * SUPERIOR_INCOMPLETO.
     */
    SUPERIOR_INCOMPLETO("SUPERIOR_INCOMPLETO"),

    /**
     * SUPERIOR_COMPLETO.
     */
    SUPERIOR_COMPLETO("SUPERIOR_COMPLETO"),

    /**
     * POS_GRADUACAO_INCOMPLETO.
     */
    POS_GRADUACAO_INCOMPLETO("POS_GRADUACAO_INCOMPLETO"),

    /**
     * POS_GRADUACAO_COMPLETO.
     */
    POS_GRADUACAO_COMPLETO("POS_GRADUACAO_COMPLETO"),

    /**
     * MESTRADO_INCOMPLETO.
     */
    MESTRADO_INCOMPLETO("MESTRADO_INCOMPLETO"),

    /**
     * MESTRADO_COMPLETO.
     */
    MESTRADO_COMPLETO("MESTRADO_COMPLETO"),

    /**
     * DOUTORADO_INCOMPLETO.
     */
    DOUTORADO_INCOMPLETO("DOUTORADO_INCOMPLETO"),

    /**
     * DOUTORADO_COMPLETO.
     */
    DOUTORADO_COMPLETO("DOUTORADO_COMPLETO");

    private final String value;

    /**
     * Cria um novo valor.
     * 
     * @param value
     *            String com o valor que deseja retornar.
     * 
     */
    Escolaridade(String value) {
        this.value = value;
    }

    /**
     * Recupera o valor.
     * 
     * @return String com o valor a ser verificado.
     */
    public String value() {
        return this.value;
    }

}
