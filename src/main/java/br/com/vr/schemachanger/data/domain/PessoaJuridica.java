/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;

import br.com.vr.schemachanger.data.converter.LocalDateTimeConverter;
import lombok.Data;

/**
 * Representa as informações que identificam uma Pessoa jurídica.
 * 
 * @author Verity
 *
 */

@Data
@DynamoDBDocument
public class PessoaJuridica implements Serializable {

    private static final long serialVersionUID = 4951866635546640241L;

    private static final String VL_PADRAO_INSCRICAO_ESTADUAL = "ISENTO";

    @DynamoDBAttribute(attributeName = "RazaoSocial")
    private String razaoSocial;

    @DynamoDBAttribute(attributeName = "NomeFantasia")
    private String nomeFantasia;

    @DynamoDBAttribute(attributeName = "InscricaoMunicipal")
    private String inscricaoMunicipal;

    @DynamoDBAttribute(attributeName = "ValorCapitalSocial")
    private Double valorCapitalSocial;

    @DynamoDBAttribute(attributeName = "InscricaoEstadual")
    private String inscricaoEstadual;

    @DynamoDBTypeConverted(converter = LocalDateTimeConverter.class)
    @DynamoDBAttribute(attributeName = "DataConstituicao")
    private LocalDate dataConstituicao;

    @DynamoDBTypeConverted(converter = LocalDateTimeConverter.class)
    @DynamoDBAttribute(attributeName = "DataFechamento")
    private LocalDate dataFechamento;

    @DynamoDBAttribute(attributeName = "MotivoFechamento")
    private String motivoFechamento;

    @DynamoDBAttribute(attributeName = "TipoSociedade")
    private String tipoSociedade;

    @DynamoDBAttribute(attributeName = "PorteEmpresa")
    private String porteEmpresa;

    @DynamoDBAttribute(attributeName = "FlagGrupoEconomico")
    private Boolean flagGrupoEconomico;

    @DynamoDBAttribute(attributeName = "TipoEmpresa")
    private String tipoEmpresa;

    @DynamoDBAttribute(attributeName = "RepresentantesLegais")
    private List<RepresentanteLegal> representantesLegais;

    @DynamoDBAttribute(attributeName = "Socios")
    private List<Socio> socios;

    @DynamoDBAttribute(attributeName = "AtividadesEconomicasPJ")
    private List<AtividadeEconomicaPj> atividadesEconomicas;

    /**
     * Set da inscricao estadual.
     * @param inscricaoEstadual
     *          Inscricao Estadual
     */
    public void setInscricaoEstadual(String inscricaoEstadual) {
        if (StringUtils.isBlank(inscricaoEstadual)) {
            this.inscricaoEstadual = VL_PADRAO_INSCRICAO_ESTADUAL;
        }
        this.inscricaoEstadual = inscricaoEstadual;
    }
    
}
