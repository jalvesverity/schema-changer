/*
 * Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger.data.domain.clienterh;

import java.io.Serializable;
import java.time.LocalDate;

import org.apache.commons.lang3.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverted;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.vr.schemachanger.data.converter.LocalDateTimeConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Entidade ProdutoContratado.
 * Representa todos os produtos contratados pelo cliente RH.
 * @author Verity
 */
@EqualsAndHashCode
@Data
@JsonPropertyOrder(value = { "siglaProduto", "codigoAplicacao", "tipoProduto", "tipoPagamento",
        "flagPermitePedidoBonus", "flagUtilizaLimite", "idContratoProduto", "status",
        "dataVencimentoContrato", "tipoEntregaCartao", "meioEntregaCartao", "tipoFaturamento",
        "flagFaturamentoLocalEntrega", "flagFaturamentoCentroCusto", "flagRecolhimentoAutomaticoSaldo",
        "regraRecolhimentoAutomaticoSaldo", "diaRecolhimentoAutomaticoSaldo", "vencimentoSaldoRecolhimentoAutomatico",
        "flagMesCheioRecolhimentoAutomatico", "regraFuncionalidadeRecolhimentoPortalBeneficiario",
        "regraFuncionalidadeRecolhimentoPortalAvulso", "regraFuncionalidadeTransferenciaPortalBeneficiario",
        "regraFuncionalidadeTransferenciaPortalAvulso", "regraConsultaExtratoBeneficiarioRh",
        "regraConsultaExtratoAvulsoRh", "tipoArquivoDescontoFolha" })
@DynamoDBDocument
public class ProdutoContratado implements Serializable {

    private static final long serialVersionUID = -9008271575255789764L;

    @DynamoDBAttribute(attributeName = "SiglaProduto")
    private String siglaProduto;

    @DynamoDBAttribute(attributeName = "CodigoAplicacao")
    private String codigoAplicacao;

    @DynamoDBAttribute(attributeName = "TipoProduto")
    private String tipoProduto;

    @DynamoDBAttribute(attributeName = "TipoPagamento")
    private String tipoPagamento;

    @DynamoDBAttribute(attributeName = "FlagPermitePedidoBonus")
    private Boolean flagPermitePedidoBonus;

    @DynamoDBAttribute(attributeName = "FlagUtilizaLimite")
    private Boolean flagUtilizaLimite;

    @DynamoDBAttribute(attributeName = "IdContratoProduto")
    private Long idContratoProduto;

    @DynamoDBAttribute(attributeName = "Status")
    private String status;

    @DynamoDBAttribute(attributeName = "DataVencimentoContrato")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    @DynamoDBTypeConverted(converter = LocalDateTimeConverter.class)
    private LocalDate dataVencimentoContrato;

    @DynamoDBAttribute(attributeName = "TipoEntregaCartao")
    private String tipoEntregaCartao;

    @DynamoDBAttribute(attributeName = "MeioEntregaCartao")
    private String meioEntregaCartao;

    @DynamoDBAttribute(attributeName = "TipoFaturamento")
    private String tipoFaturamento;

    @DynamoDBAttribute(attributeName = "FlagFaturamentoLocalEntrega")
    private Boolean flagFaturamentoLocalEntrega;

    @DynamoDBAttribute(attributeName = "FlagFaturamentoCentroCusto")
    private Boolean flagFaturamentoCentroCusto;

    @DynamoDBAttribute(attributeName = "FlagRecolhimentoAutomaticoSaldo")
    private Boolean flagRecolhimentoAutomaticoSaldo;

    @DynamoDBAttribute(attributeName = "RegraRecolhimentoAutomaticoSaldo")
    private String regraRecolhimentoAutomaticoSaldo;

    @DynamoDBAttribute(attributeName = "DiaRecolhimentoAutomaticoSaldo")
    private Integer diaRecolhimentoAutomaticoSaldo;

    @DynamoDBAttribute(attributeName = "VencimentoSaldoRecolhimentoAutomatico")
    private Integer vencimentoSaldoRecolhimentoAutomatico;

    @DynamoDBAttribute(attributeName = "FlagMesCheioRecolhimentoAutomatico")
    private Boolean flagMesCheioRecolhimentoAutomatico;

    @DynamoDBAttribute(attributeName = "RegraFuncionalidadeRecolhimentoPortalBeneficiario")
    private String regraFuncionalidadeRecolhimentoPortalBeneficiario;

    @DynamoDBAttribute(attributeName = "RegraFuncionalidadeRecolhimentoPortalAvulso")
    private String regraFuncionalidadeRecolhimentoPortalAvulso;

    @DynamoDBAttribute(attributeName = "RegraFuncionalidadeTransferenciaPortalBeneficiario")
    private String regraFuncionalidadeTransferenciaPortalBeneficiario;

    @DynamoDBAttribute(attributeName = "RegraFuncionalidadeTransferenciaPortalAvulso")
    private String regraFuncionalidadeTransferenciaPortalAvulso;

    @DynamoDBAttribute(attributeName = "RegraConsultaExtratoBeneficiarioRH")
    private String regraConsultaExtratoBeneficiarioRh;

    @DynamoDBAttribute(attributeName = "RegraConsultaExtratoAvulsoRH")
    private String regraConsultaExtratoAvulsoRh;

    @DynamoDBAttribute(attributeName = "TipoArquivoDescontoFolha")
    private String tipoArquivoDescontoFolha;

    public void setSiglaProduto(String siglaProduto) {
        this.siglaProduto = StringUtils.upperCase(StringUtils.trimToEmpty(siglaProduto));
    }
}
