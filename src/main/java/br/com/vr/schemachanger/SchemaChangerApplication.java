/*
* Copyright(c) by VR Benefícios
*
* All rights reserved.
*
* This software is confidential and proprietary information of
* VR Benefícios ("Confidential Information").
* You shall not disclose such Confidential Information and shall
* use it only in accordance with the terms of the license agreement
* you entered with VR Benefícios.
*/

package br.com.vr.schemachanger;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import br.com.vr.schemachanger.changer.ClienteRhChanger;
import br.com.vr.schemachanger.changer.PessoaChanger;

/**
 * Classe utilizada para inicializar a aplicação.
 *
 * @author Verity
 * 
 */
@SpringBootApplication
public class SchemaChangerApplication implements CommandLineRunner {

    @Autowired
    private PessoaChanger pessoaChanger;

    @Autowired
    private ClienteRhChanger clienteChanger;

    @Value("${migration.target.database}")
    private String target;

    private static final String PESSOA = "pessoa";
    private static final String CLIENTE = "cliente";

    /**
     * Método de inicialização.
     * 
     * @param args
     *            String[] - Parâmetros que são passados na linha de comando ao
     *            inicializar a aplicação.
     */
    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(SchemaChangerApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.run(args);
    }

    @Override
    public void run(String... args) throws Exception {

        if (target.equals(PESSOA)) {
            pessoaChanger.copiaDadosTabelaPessoa();
        }

        if (target.equals(CLIENTE)) {
            clienteChanger.copiaDadosTabelaClienteRh();
        }
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
